## Architecture Overview

```mermaid
graph LR
A(Widgets) --> B(Stores)
B --> C(Repositories)
B --> D
C --> D(Services)
```
## Tech Stack
 - `mobx` for Stores
 - `chopper` for rest services
 - `built_value` for object modeling and JSON serialization
 - `hive` for local storage


## Complimentary Tech Stack
 - `provider` is used to inject stores into widgets
 - `get_it` is used to inject services and repositories, and anything singleton 