import 'package:flutter/material.dart';
import 'package:flutter_mvvm_architecture/screens/home/article_store.dart';
import 'package:flutter_mvvm_architecture/screens/home/home_page.dart';
import 'package:flutter_mvvm_architecture/screens/splash/splash_page.dart';
import 'package:flutter_mvvm_architecture/shared/stores/connectivity_store.dart';
import 'package:flutter_mvvm_architecture/shared/stores/root_store.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import 'initializers.dart';

void main() {
  setUpLogging();
  initLocator();
  initHive();
  runApp(MyApp());
}

void setUpLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print("${rec.level.name}: ${rec.time}: ${rec.message}");
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // TODO figure out a way to inject rootstore into other stores that depend on it
        // One way would be using ProxyProvider
        Provider<ConnectivityStore>(
          create: (_) => ConnectivityStore(),
        ),
        Provider<RootStore>(
          create: (_) => RootStore(),
        ),
        Provider<ArticleStore>(
          create: (_) => ArticleStore(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: SplashPage.routeName,
        routes: {
          SplashPage.routeName: (context) => SplashPage(),
          HomePage.routeName: (context) => HomePage(),
        },
      ),
    );
  }
}
