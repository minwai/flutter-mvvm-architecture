import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvvm_architecture/screens/home/home_page.dart';
import 'package:flutter_mvvm_architecture/shared/stores/connectivity_store.dart';
import 'package:flutter_mvvm_architecture/utils/core_utils.dart';
import 'package:mobx/mobx.dart';

class SplashPage extends StatefulWidget {
  static const String routeName = "/";

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  ConnectivityStore _connectivityStore;
  List<ReactionDisposer> disposers = [];

  @override
  void initState() {
    super.initState();
    _connectivityStore = getStore<ConnectivityStore>(context);
    Future.microtask(() {
      disposers.add(onNetworkChecked());
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (final disposer in disposers) {
      disposer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void _navigateToHome() {
    Navigator.pushReplacementNamed(context, HomePage.routeName);
  }

  // TODO change return type to void since when clause auto disposes itself
  ReactionDisposer onNetworkChecked() {
    return when(
      (_) {
        chopperLogger.info(
            "Connectivity service is ${_connectivityStore.connectivity.status == StreamStatus.active ? 'ready' : 'pending'}.");
        return _connectivityStore.connectivity.status == StreamStatus.active;
      },
      () {
        chopperLogger.info("Connection check succeeded.");
        _navigateToHome();
      },
    );
  }
}
