import 'package:chopper/chopper.dart';
import 'package:flutter_mvvm_architecture/locator.dart';
import 'package:flutter_mvvm_architecture/shared/models/app_exception.dart';
import 'package:flutter_mvvm_architecture/shared/models/article.dart';
import 'package:flutter_mvvm_architecture/shared/services/rest_service.dart';

class ArticleRepository {
  final RestService restService = locator<RestService>();

  /// Refresh new articles from [RestService] and store the result in
  /// [StorageService] if successful
  Future<List<Article>> refreshArticles(String csrf) async {
    try {
      final response = await restService.fetchArticles(csrf);
      final articleWrapper = response.body;
      final articles = articleWrapper.articles;
      // TODO if fetching articles is successful, cache them in local db
      // return an immutable list
      return articles.asList();
    } catch (e) {
      // When api call fails, load data from db and throw a custom exception with loaded data
      chopperLogger.warning(e.toString());
      final articles = <Article>[];
      // TODO use different messages for different exceptions
      throw AppException(message: "Something went wrong.", data: articles);
    }
  }

  void dispose() {}
}
