import 'dart:math' as math;
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_mvvm_architecture/shared/models/article.dart';
import 'package:flutter_mvvm_architecture/shared/stores/connectivity_store.dart';
import 'package:flutter_mvvm_architecture/shared/stores/root_store.dart';
import 'package:flutter_mvvm_architecture/utils/core_utils.dart';
import 'package:mobx/mobx.dart';

import 'article_store.dart';

class HomePage extends StatefulWidget {
  static const String routeName = "/home/";
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<ReactionDisposer> _disposers = [];
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorState = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  ConnectivityStore _connectivityStore;
  RootStore _rootStore;
  ArticleStore _articleStore;

  @override
  void initState() {
    super.initState();
    _connectivityStore = getStore<ConnectivityStore>(context);
    _rootStore = getStore<RootStore>(context);
    _articleStore = getStore<ArticleStore>(context);

    /// Startup Logic goes here
    Future.microtask(() {
      _disposers.add(_onNetworkChanged());
      _disposers.add(_onNewCSRF());
      _disposers.add(_onError());
      // _onStartUp();
      // disposers.add(_onLoading());
    });
    // disposers.add(_onNetworkAvailable());
    // disposers.add(_onNewCSRF());
    // disposers.add(_onError());
  }

  @override
  void dispose() {
    super.dispose();

    for (final disposer in _disposers) {
      disposer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Home"),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Stack(
      children: <Widget>[
        Observer(
          builder: (context) {
            return RefreshIndicator(
              key: _refreshIndicatorState,
              onRefresh: () => _articleStore.fetchArticles(_rootStore.csrf),
              child: Stack(
                children: <Widget>[
                  _buildArticles(_articleStore, _rootStore),
                  if (!_articleStore.hasResults)
                    _buildEmptyArticles()
                  // For empty articles state
                  // TODO integrate both isLoading into one computed isLoading to free businees
                  // logic in this widget
                ],
              ),
            );
          },
        )
      ],
    );
  }

  Widget _buildArticles(ArticleStore store, RootStore rootStore) {
    return ListView.builder(
      itemCount: store.articles.length,
      itemBuilder: (context, index) {
        return _buildArticleRow(store.articles[index]);
      },
    );
  }

  Widget _buildEmptyArticles() {
    return const Center(
      child: Text("There are no articles for now."),
    );
  }

  Widget _buildArticleRow(Article article) {
    return Card(
      child: ListTile(
        title: Text(article.title),
        subtitle: Text(
            article.detail.substring(0, math.min(100, article.detail.length))),
      ),
    );
  }

  // TODO separate these methods into a different file or class
  // since they will be used on almost all pages
  ReactionDisposer _onNetworkChanged() {
    // Runs immediately once and more on later changes
    return autorun((r) {
      chopperLogger.info("Network is ${_connectivityStore.isNetworkOn}");
      _connectivityStore.isNetworkOn
          ? _rootStore.renewCSRF()
          : _articleStore.fetchArticles(_rootStore.csrf);
    });
  }

  ReactionDisposer _onError() {
    return reaction<String>((_) => _articleStore.error, (result) {
      if (result.isNotEmpty) {
        chopperLogger.info("showing error in ui $result");
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("$result"),
        ));
      }
    });
  }

  ReactionDisposer _onNewCSRF() {
    return reaction<String>((_) => _rootStore.csrf, (csrf) {
      if (csrf.isNotEmpty) {
        chopperLogger.info("New CSRF key is $csrf");
        // _articleStore.fetchArticles(csrf);
        // Call _refreshIndicator's [show] to load articles and reflect loading state on ui
        _refreshIndicatorState.currentState.show();
      }
    });
  }
}
