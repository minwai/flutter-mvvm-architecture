// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ArticleStore on _ArticleStoreBase, Store {
  Computed<List<Article>> _$articlesComputed;

  @override
  List<Article> get articles =>
      (_$articlesComputed ??= Computed<List<Article>>(() => super.articles))
          .value;
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading)).value;
  Computed<String> _$errorComputed;

  @override
  String get error =>
      (_$errorComputed ??= Computed<String>(() => super.error)).value;
  Computed<bool> _$hasResultsComputed;

  @override
  bool get hasResults =>
      (_$hasResultsComputed ??= Computed<bool>(() => super.hasResults)).value;

  final _$_articlesAtom = Atom(name: '_ArticleStoreBase._articles');

  @override
  ObservableFuture<List<Article>> get _articles {
    _$_articlesAtom.context.enforceReadPolicy(_$_articlesAtom);
    _$_articlesAtom.reportObserved();
    return super._articles;
  }

  @override
  set _articles(ObservableFuture<List<Article>> value) {
    _$_articlesAtom.context.conditionallyRunInAction(() {
      super._articles = value;
      _$_articlesAtom.reportChanged();
    }, _$_articlesAtom, name: '${_$_articlesAtom.name}_set');
  }

  final _$fetchArticlesAsyncAction = AsyncAction('fetchArticles');

  @override
  Future<dynamic> fetchArticles(String csrf) {
    return _$fetchArticlesAsyncAction.run(() => super.fetchArticles(csrf));
  }
}
