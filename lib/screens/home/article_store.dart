import 'package:chopper/chopper.dart';
import 'package:flutter_mvvm_architecture/locator.dart';
import 'package:flutter_mvvm_architecture/shared/models/app_exception.dart';
import 'package:flutter_mvvm_architecture/shared/models/article.dart';
import 'package:mobx/mobx.dart';

import 'article_repository.dart';

part 'article_store.g.dart';

class ArticleStore = _ArticleStoreBase with _$ArticleStore;

abstract class _ArticleStoreBase with Store {
  final ArticleRepository _articleRepository = locator<ArticleRepository>();

  /// A list of articles fetched from api
  /// kept private to manipulate based on its result
  @observable
  ObservableFuture<List<Article>> _articles = ObservableFuture.value([]);

  /// Articles cached in [fetchArticles] method
  List<Article> _cachedArticles = [];

  @computed
  List<Article> get articles {
    final articles = _articles.result;

    /// If articles is an app exception, get only data
    if (articles is AppException<List<Article>>) {
      return articles.data;
    }

    /// While fetches are pending, articles will be null
    /// return _cachedArticles in that case
    return articles as List<Article> ?? _cachedArticles;
  }

  @computed
  bool get isLoading => _articles.status == FutureStatus.pending;

  @computed
  String get error {
    return _articles.status == FutureStatus.rejected
        ? _articles.error.toString()
        : "";
  }

  @computed
  bool get hasResults {
    return articles.isNotEmpty;
  }

  @action
  Future fetchArticles(String csrf) async {
    chopperLogger.info("Fetching articles");

    /// Renew cached articles before proceeding with request
    _cachedArticles = articles;
    _articles = ObservableFuture(_articleRepository.refreshArticles(csrf));

    // return await _articleList.catchError((error) => print(error));
    return await _articles.catchError(chopperLogger.warning);
  }
}
