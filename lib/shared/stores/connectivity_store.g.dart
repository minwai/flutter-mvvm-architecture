// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'connectivity_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ConnectivityStore on _ConnectivityStoreBase, Store {
  Computed<bool> _$isNetworkOnComputed;

  @override
  bool get isNetworkOn =>
      (_$isNetworkOnComputed ??= Computed<bool>(() => super.isNetworkOn)).value;
  Computed<String> _$messageComputed;

  @override
  String get message =>
      (_$messageComputed ??= Computed<String>(() => super.message)).value;

  final _$connectivityAtom = Atom(name: '_ConnectivityStoreBase.connectivity');

  @override
  ObservableStream<bool> get connectivity {
    _$connectivityAtom.context.enforceReadPolicy(_$connectivityAtom);
    _$connectivityAtom.reportObserved();
    return super.connectivity;
  }

  @override
  set connectivity(ObservableStream<bool> value) {
    _$connectivityAtom.context.conditionallyRunInAction(() {
      super.connectivity = value;
      _$connectivityAtom.reportChanged();
    }, _$connectivityAtom, name: '${_$connectivityAtom.name}_set');
  }
}
