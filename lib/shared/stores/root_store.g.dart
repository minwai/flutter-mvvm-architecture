// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'root_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RootStore on _RootStoreBase, Store {
  Computed<String> _$csrfComputed;

  @override
  String get csrf =>
      (_$csrfComputed ??= Computed<String>(() => super.csrf)).value;
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading)).value;

  final _$_responseAtom = Atom(name: '_RootStoreBase._response');

  @override
  ObservableFuture<Response<dynamic>> get _response {
    _$_responseAtom.context.enforceReadPolicy(_$_responseAtom);
    _$_responseAtom.reportObserved();
    return super._response;
  }

  @override
  set _response(ObservableFuture<Response<dynamic>> value) {
    _$_responseAtom.context.conditionallyRunInAction(() {
      super._response = value;
      _$_responseAtom.reportChanged();
    }, _$_responseAtom, name: '${_$_responseAtom.name}_set');
  }

  final _$renewCSRFAsyncAction = AsyncAction('renewCSRF');

  @override
  Future<dynamic> renewCSRF() {
    return _$renewCSRFAsyncAction.run(() => super.renewCSRF());
  }
}
