import 'package:chopper/chopper.dart';
import 'package:flutter_mvvm_architecture/shared/services/rest_service.dart';
import 'package:flutter_mvvm_architecture/utils/constants.dart';
import 'package:mobx/mobx.dart';

import '../../locator.dart';

part 'root_store.g.dart';

class RootStore = _RootStoreBase with _$RootStore;

abstract class _RootStoreBase with Store {
  // TODO create a new root repository
  // TODO implement error handling for failed response
  final RestService _restService = locator<RestService>();

  /// A pending response at app startup
  @observable
  ObservableFuture<Response> _response =
      ObservableFuture(Future.delayed(const Duration(seconds: 10)));

  /// Returns CSRF if [_response] is successful or an empty string otherwise
  @computed
  String get csrf {
    if (_response.status == FutureStatus.fulfilled) {
      return _response.value.headers[Constants.csrfKey];
    }
    return "";
  }

  @computed
  bool get isLoading => _response.status == FutureStatus.pending;

  @action
  Future renewCSRF() async {
    // TODO implement a new repository named root repository where error handling and business
    // logic will happen
    _response = ObservableFuture(_restService.fetchCSRF());
    await _response.catchError(chopperLogger.warning);
  }
}
