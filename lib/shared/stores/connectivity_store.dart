import 'package:flutter_mvvm_architecture/locator.dart';
import 'package:flutter_mvvm_architecture/shared/services/connectivity_service.dart';
import 'package:mobx/mobx.dart';
part 'connectivity_store.g.dart';

class ConnectivityStore = _ConnectivityStoreBase with _$ConnectivityStore;

abstract class _ConnectivityStoreBase with Store {
  @observable
  ObservableStream<bool> connectivity = ObservableStream(
    locator<ConnectivityService>().connectivityStream,
  );

  @computed
  bool get isNetworkOn => connectivity.value ?? false;

  @computed
  String get message =>
      connectivity.hasError ? "You're offline" : "You're online";
}
