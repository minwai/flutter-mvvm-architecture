library article_wrapper;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_mvvm_architecture/shared/models/article.dart';
import 'package:flutter_mvvm_architecture/utils/serializers.dart';

part 'article_wrapper.g.dart';

abstract class ArticleWrapper
    implements Built<ArticleWrapper, ArticleWrapperBuilder> {
  factory ArticleWrapper([Function(ArticleWrapperBuilder b) updates]) =
      _$ArticleWrapper;
  ArticleWrapper._();

  @BuiltValueField(wireName: 'article')
  BuiltList<Article> get articles;
  String toJson() {
    return json
        .encode(serializers.serializeWith(ArticleWrapper.serializer, this));
  }

  static ArticleWrapper fromJson(String jsonString) {
    return serializers.deserializeWith(
        ArticleWrapper.serializer, json.decode(jsonString));
  }

  static Serializer<ArticleWrapper> get serializer =>
      _$articleWrapperSerializer;
}
