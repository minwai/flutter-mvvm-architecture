// GENERATED CODE - DO NOT MODIFY BY HAND

part of article_wrapper;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ArticleWrapper> _$articleWrapperSerializer =
    new _$ArticleWrapperSerializer();

class _$ArticleWrapperSerializer
    implements StructuredSerializer<ArticleWrapper> {
  @override
  final Iterable<Type> types = const [ArticleWrapper, _$ArticleWrapper];
  @override
  final String wireName = 'ArticleWrapper';

  @override
  Iterable<Object> serialize(Serializers serializers, ArticleWrapper object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'article',
      serializers.serialize(object.articles,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Article)])),
    ];

    return result;
  }

  @override
  ArticleWrapper deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ArticleWrapperBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'article':
          result.articles.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Article)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$ArticleWrapper extends ArticleWrapper {
  @override
  final BuiltList<Article> articles;

  factory _$ArticleWrapper([void Function(ArticleWrapperBuilder) updates]) =>
      (new ArticleWrapperBuilder()..update(updates)).build();

  _$ArticleWrapper._({this.articles}) : super._() {
    if (articles == null) {
      throw new BuiltValueNullFieldError('ArticleWrapper', 'articles');
    }
  }

  @override
  ArticleWrapper rebuild(void Function(ArticleWrapperBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ArticleWrapperBuilder toBuilder() =>
      new ArticleWrapperBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ArticleWrapper && articles == other.articles;
  }

  @override
  int get hashCode {
    return $jf($jc(0, articles.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ArticleWrapper')
          ..add('articles', articles))
        .toString();
  }
}

class ArticleWrapperBuilder
    implements Builder<ArticleWrapper, ArticleWrapperBuilder> {
  _$ArticleWrapper _$v;

  ListBuilder<Article> _articles;
  ListBuilder<Article> get articles =>
      _$this._articles ??= new ListBuilder<Article>();
  set articles(ListBuilder<Article> articles) => _$this._articles = articles;

  ArticleWrapperBuilder();

  ArticleWrapperBuilder get _$this {
    if (_$v != null) {
      _articles = _$v.articles?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ArticleWrapper other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ArticleWrapper;
  }

  @override
  void update(void Function(ArticleWrapperBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ArticleWrapper build() {
    _$ArticleWrapper _$result;
    try {
      _$result = _$v ?? new _$ArticleWrapper._(articles: articles.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'articles';
        articles.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ArticleWrapper', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
