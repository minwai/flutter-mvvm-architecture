library article;

import 'dart:convert';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_mvvm_architecture/utils/serializers.dart';
import 'package:hive/hive.dart';

part 'article.g.dart';

@HiveType(typeId: 0)
abstract class Article implements Built<Article, ArticleBuilder> {
  factory Article([Function(ArticleBuilder b) updates]) = _$Article;
  Article._();

  @HiveField(0)
  @BuiltValueField(wireName: 'id')
  int get id;

  @HiveField(1)
  @BuiltValueField(wireName: 'article_category')
  int get articleCategory;

  @HiveField(2)
  @BuiltValueField(wireName: 'title')
  String get title;

  @HiveField(3)
  @BuiltValueField(wireName: 'detail')
  String get detail;

  @HiveField(4)
  @BuiltValueField(wireName: 'coverimage')
  String get coverimage;

  @HiveField(5)
  @BuiltValueField(wireName: 'status')
  int get status;

  @HiveField(6)
  @BuiltValueField(wireName: 'article_like')
  int get articleLike;

  @HiveField(7)
  @BuiltValueField(wireName: 'article_comment')
  int get articleComment;

  @HiveField(8)
  @BuiltValueField(wireName: 'article_viewcount')
  int get articleViewcount;

  @HiveField(9)
  @BuiltValueField(wireName: 'promotion_article')
  String get promotionArticle;

  // @HiveField(10)
  @nullable
  @BuiltValueField(wireName: 'promotion_from')
  String get promotionFrom;

  // @HiveField(11)
  @nullable
  @BuiltValueField(wireName: 'promotion_expire')
  String get promotionExpire;

  @HiveField(12)
  @BuiltValueField(wireName: 'userid')
  int get userid;

  @HiveField(13)
  @BuiltValueField(wireName: 'created_date')
  String get createdDate;

  @HiveField(14)
  @BuiltValueField(wireName: 'updated_date')
  String get updatedDate;

  @HiveField(15)
  @BuiltValueField(wireName: 'category')
  String get category;

  @HiveField(16)
  @BuiltValueField(wireName: 'actions')
  String get actions;

  String toJson() {
    return json.encode(serializers.serializeWith(Article.serializer, this));
  }

  static Article fromJson(String jsonString) {
    return serializers.deserializeWith(
        Article.serializer, json.decode(jsonString));
  }

  static Serializer<Article> get serializer => _$articleSerializer;
}
