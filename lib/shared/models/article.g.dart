// GENERATED CODE - DO NOT MODIFY BY HAND

part of article;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Article> _$articleSerializer = new _$ArticleSerializer();

class _$ArticleSerializer implements StructuredSerializer<Article> {
  @override
  final Iterable<Type> types = const [Article, _$Article];
  @override
  final String wireName = 'Article';

  @override
  Iterable<Object> serialize(Serializers serializers, Article object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'article_category',
      serializers.serialize(object.articleCategory,
          specifiedType: const FullType(int)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'detail',
      serializers.serialize(object.detail,
          specifiedType: const FullType(String)),
      'coverimage',
      serializers.serialize(object.coverimage,
          specifiedType: const FullType(String)),
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(int)),
      'article_like',
      serializers.serialize(object.articleLike,
          specifiedType: const FullType(int)),
      'article_comment',
      serializers.serialize(object.articleComment,
          specifiedType: const FullType(int)),
      'article_viewcount',
      serializers.serialize(object.articleViewcount,
          specifiedType: const FullType(int)),
      'promotion_article',
      serializers.serialize(object.promotionArticle,
          specifiedType: const FullType(String)),
      'userid',
      serializers.serialize(object.userid, specifiedType: const FullType(int)),
      'created_date',
      serializers.serialize(object.createdDate,
          specifiedType: const FullType(String)),
      'updated_date',
      serializers.serialize(object.updatedDate,
          specifiedType: const FullType(String)),
      'category',
      serializers.serialize(object.category,
          specifiedType: const FullType(String)),
      'actions',
      serializers.serialize(object.actions,
          specifiedType: const FullType(String)),
    ];
    if (object.promotionFrom != null) {
      result
        ..add('promotion_from')
        ..add(serializers.serialize(object.promotionFrom,
            specifiedType: const FullType(String)));
    }
    if (object.promotionExpire != null) {
      result
        ..add('promotion_expire')
        ..add(serializers.serialize(object.promotionExpire,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Article deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ArticleBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'article_category':
          result.articleCategory = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'detail':
          result.detail = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coverimage':
          result.coverimage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'article_like':
          result.articleLike = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'article_comment':
          result.articleComment = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'article_viewcount':
          result.articleViewcount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'promotion_article':
          result.promotionArticle = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'promotion_from':
          result.promotionFrom = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'promotion_expire':
          result.promotionExpire = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userid':
          result.userid = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'created_date':
          result.createdDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_date':
          result.updatedDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'category':
          result.category = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'actions':
          result.actions = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Article extends Article {
  @override
  final int id;
  @override
  final int articleCategory;
  @override
  final String title;
  @override
  final String detail;
  @override
  final String coverimage;
  @override
  final int status;
  @override
  final int articleLike;
  @override
  final int articleComment;
  @override
  final int articleViewcount;
  @override
  final String promotionArticle;
  @override
  final String promotionFrom;
  @override
  final String promotionExpire;
  @override
  final int userid;
  @override
  final String createdDate;
  @override
  final String updatedDate;
  @override
  final String category;
  @override
  final String actions;

  factory _$Article([void Function(ArticleBuilder) updates]) =>
      (new ArticleBuilder()..update(updates)).build();

  _$Article._(
      {this.id,
      this.articleCategory,
      this.title,
      this.detail,
      this.coverimage,
      this.status,
      this.articleLike,
      this.articleComment,
      this.articleViewcount,
      this.promotionArticle,
      this.promotionFrom,
      this.promotionExpire,
      this.userid,
      this.createdDate,
      this.updatedDate,
      this.category,
      this.actions})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Article', 'id');
    }
    if (articleCategory == null) {
      throw new BuiltValueNullFieldError('Article', 'articleCategory');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('Article', 'title');
    }
    if (detail == null) {
      throw new BuiltValueNullFieldError('Article', 'detail');
    }
    if (coverimage == null) {
      throw new BuiltValueNullFieldError('Article', 'coverimage');
    }
    if (status == null) {
      throw new BuiltValueNullFieldError('Article', 'status');
    }
    if (articleLike == null) {
      throw new BuiltValueNullFieldError('Article', 'articleLike');
    }
    if (articleComment == null) {
      throw new BuiltValueNullFieldError('Article', 'articleComment');
    }
    if (articleViewcount == null) {
      throw new BuiltValueNullFieldError('Article', 'articleViewcount');
    }
    if (promotionArticle == null) {
      throw new BuiltValueNullFieldError('Article', 'promotionArticle');
    }
    if (userid == null) {
      throw new BuiltValueNullFieldError('Article', 'userid');
    }
    if (createdDate == null) {
      throw new BuiltValueNullFieldError('Article', 'createdDate');
    }
    if (updatedDate == null) {
      throw new BuiltValueNullFieldError('Article', 'updatedDate');
    }
    if (category == null) {
      throw new BuiltValueNullFieldError('Article', 'category');
    }
    if (actions == null) {
      throw new BuiltValueNullFieldError('Article', 'actions');
    }
  }

  @override
  Article rebuild(void Function(ArticleBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ArticleBuilder toBuilder() => new ArticleBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Article &&
        id == other.id &&
        articleCategory == other.articleCategory &&
        title == other.title &&
        detail == other.detail &&
        coverimage == other.coverimage &&
        status == other.status &&
        articleLike == other.articleLike &&
        articleComment == other.articleComment &&
        articleViewcount == other.articleViewcount &&
        promotionArticle == other.promotionArticle &&
        promotionFrom == other.promotionFrom &&
        promotionExpire == other.promotionExpire &&
        userid == other.userid &&
        createdDate == other.createdDate &&
        updatedDate == other.updatedDate &&
        category == other.category &&
        actions == other.actions;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        0,
                                                                        id
                                                                            .hashCode),
                                                                    articleCategory
                                                                        .hashCode),
                                                                title.hashCode),
                                                            detail.hashCode),
                                                        coverimage.hashCode),
                                                    status.hashCode),
                                                articleLike.hashCode),
                                            articleComment.hashCode),
                                        articleViewcount.hashCode),
                                    promotionArticle.hashCode),
                                promotionFrom.hashCode),
                            promotionExpire.hashCode),
                        userid.hashCode),
                    createdDate.hashCode),
                updatedDate.hashCode),
            category.hashCode),
        actions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Article')
          ..add('id', id)
          ..add('articleCategory', articleCategory)
          ..add('title', title)
          ..add('detail', detail)
          ..add('coverimage', coverimage)
          ..add('status', status)
          ..add('articleLike', articleLike)
          ..add('articleComment', articleComment)
          ..add('articleViewcount', articleViewcount)
          ..add('promotionArticle', promotionArticle)
          ..add('promotionFrom', promotionFrom)
          ..add('promotionExpire', promotionExpire)
          ..add('userid', userid)
          ..add('createdDate', createdDate)
          ..add('updatedDate', updatedDate)
          ..add('category', category)
          ..add('actions', actions))
        .toString();
  }
}

class ArticleBuilder implements Builder<Article, ArticleBuilder> {
  _$Article _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _articleCategory;
  int get articleCategory => _$this._articleCategory;
  set articleCategory(int articleCategory) =>
      _$this._articleCategory = articleCategory;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _detail;
  String get detail => _$this._detail;
  set detail(String detail) => _$this._detail = detail;

  String _coverimage;
  String get coverimage => _$this._coverimage;
  set coverimage(String coverimage) => _$this._coverimage = coverimage;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  int _articleLike;
  int get articleLike => _$this._articleLike;
  set articleLike(int articleLike) => _$this._articleLike = articleLike;

  int _articleComment;
  int get articleComment => _$this._articleComment;
  set articleComment(int articleComment) =>
      _$this._articleComment = articleComment;

  int _articleViewcount;
  int get articleViewcount => _$this._articleViewcount;
  set articleViewcount(int articleViewcount) =>
      _$this._articleViewcount = articleViewcount;

  String _promotionArticle;
  String get promotionArticle => _$this._promotionArticle;
  set promotionArticle(String promotionArticle) =>
      _$this._promotionArticle = promotionArticle;

  String _promotionFrom;
  String get promotionFrom => _$this._promotionFrom;
  set promotionFrom(String promotionFrom) =>
      _$this._promotionFrom = promotionFrom;

  String _promotionExpire;
  String get promotionExpire => _$this._promotionExpire;
  set promotionExpire(String promotionExpire) =>
      _$this._promotionExpire = promotionExpire;

  int _userid;
  int get userid => _$this._userid;
  set userid(int userid) => _$this._userid = userid;

  String _createdDate;
  String get createdDate => _$this._createdDate;
  set createdDate(String createdDate) => _$this._createdDate = createdDate;

  String _updatedDate;
  String get updatedDate => _$this._updatedDate;
  set updatedDate(String updatedDate) => _$this._updatedDate = updatedDate;

  String _category;
  String get category => _$this._category;
  set category(String category) => _$this._category = category;

  String _actions;
  String get actions => _$this._actions;
  set actions(String actions) => _$this._actions = actions;

  ArticleBuilder();

  ArticleBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _articleCategory = _$v.articleCategory;
      _title = _$v.title;
      _detail = _$v.detail;
      _coverimage = _$v.coverimage;
      _status = _$v.status;
      _articleLike = _$v.articleLike;
      _articleComment = _$v.articleComment;
      _articleViewcount = _$v.articleViewcount;
      _promotionArticle = _$v.promotionArticle;
      _promotionFrom = _$v.promotionFrom;
      _promotionExpire = _$v.promotionExpire;
      _userid = _$v.userid;
      _createdDate = _$v.createdDate;
      _updatedDate = _$v.updatedDate;
      _category = _$v.category;
      _actions = _$v.actions;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Article other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Article;
  }

  @override
  void update(void Function(ArticleBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Article build() {
    final _$result = _$v ??
        new _$Article._(
            id: id,
            articleCategory: articleCategory,
            title: title,
            detail: detail,
            coverimage: coverimage,
            status: status,
            articleLike: articleLike,
            articleComment: articleComment,
            articleViewcount: articleViewcount,
            promotionArticle: promotionArticle,
            promotionFrom: promotionFrom,
            promotionExpire: promotionExpire,
            userid: userid,
            createdDate: createdDate,
            updatedDate: updatedDate,
            category: category,
            actions: actions);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ArticleAdapter extends TypeAdapter<Article> {
  @override
  final int typeId = 0;

  @override
  Article read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Article((b) => b
      ..id = fields[0] as int
      ..articleCategory = fields[1] as int
      ..title = fields[2] as String
      ..detail = fields[3] as String
      ..coverimage = fields[4] as String
      ..status = fields[5] as int
      ..articleLike = fields[6] as int
      ..articleComment = fields[7] as int
      ..articleViewcount = fields[8] as int
      ..promotionArticle = fields[9] as String
      ..userid = fields[12] as int
      ..createdDate = fields[13] as String
      ..updatedDate = fields[14] as String
      ..category = fields[15] as String
      ..actions = fields[16] as String);
  }

  @override
  void write(BinaryWriter writer, Article obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.articleCategory)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.detail)
      ..writeByte(4)
      ..write(obj.coverimage)
      ..writeByte(5)
      ..write(obj.status)
      ..writeByte(6)
      ..write(obj.articleLike)
      ..writeByte(7)
      ..write(obj.articleComment)
      ..writeByte(8)
      ..write(obj.articleViewcount)
      ..writeByte(9)
      ..write(obj.promotionArticle)
      ..writeByte(12)
      ..write(obj.userid)
      ..writeByte(13)
      ..write(obj.createdDate)
      ..writeByte(14)
      ..write(obj.updatedDate)
      ..writeByte(15)
      ..write(obj.category)
      ..writeByte(16)
      ..write(obj.actions);
  }
}
