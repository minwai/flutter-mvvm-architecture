import 'package:flutter/foundation.dart';

class AppException<T> implements Exception {
  final String message;
  final T data;

  AppException({@required this.message, @required this.data});

  @override
  String toString() {
    return "$message";
  }
}

// class FetchDataException<T> extends AppException {
//   FetchDataException({@required T data})
//       : super(message: "Error During Communication: ", data: data);
// }

// class BadRequestException<T> extends AppException {
//   BadRequestException({@required T data})
//       : super(message: "Invalid Request: ", data: data);
// }

// class UnauthorisedException<T> extends AppException {
//   UnauthorisedException({@required T data})
//       : super(message: "Unauthorised: ", data: data);
// }

// class InvalidInputException<T> extends AppException {
//   InvalidInputException({@required T data})
//       : super(message: "Invalid Input: ", data: data);
// }
