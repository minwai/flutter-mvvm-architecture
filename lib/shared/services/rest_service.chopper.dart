// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$RestService extends RestService {
  _$RestService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = RestService;

  @override
  Future<Response> fetchCSRF() {
    final $url = '/restapi';
    final $headers = {'authorization': 'Basic c3lvOndvcmQ'};
    final $request = Request('HEAD', $url, client.baseUrl, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<ArticleWrapper>> fetchArticles(String _csrf,
      {String columns = Constants.defaultColumns,
      String join = Constants.defaultJoin,
      String filter = Constants.defaultFilter,
      String order = Constants.defaultOrder,
      String page = Constants.defaultPageRange}) {
    final $url = '/restapi/article';
    final $params = <String, dynamic>{
      '_csrf': _csrf,
      'columns': columns,
      'join': join,
      'filter': filter,
      'order': order,
      'page': page
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<ArticleWrapper, ArticleWrapper>($request);
  }
}
