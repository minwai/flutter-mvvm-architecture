import 'dart:io';
import 'package:chopper/chopper.dart';
import 'package:flutter_mvvm_architecture/shared/models/article_wrapper.dart';
import 'package:flutter_mvvm_architecture/utils/constants.dart';

part 'rest_service.chopper.dart';

@ChopperApi(baseUrl: '/restapi')
abstract class RestService extends ChopperService {
  static RestService create([ChopperClient client]) => _$RestService(client);

  // RestService._();

  @Head(headers: {HttpHeaders.authorizationHeader: "Basic c3lvOndvcmQ"})
  Future<Response> fetchCSRF();

  @Get(path: "/article")
  Future<Response<ArticleWrapper>> fetchArticles(
    @Query() String _csrf, {
    @Query() String columns = Constants.defaultColumns,
    @Query() String join = Constants.defaultJoin,
    @Query() String filter = Constants.defaultFilter,
    @Query() String order = Constants.defaultOrder,
    @Query() String page = Constants.defaultPageRange,
  });
}
