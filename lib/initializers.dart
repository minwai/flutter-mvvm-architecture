import 'package:chopper/chopper.dart';
import 'package:flutter_mvvm_architecture/locator.dart';
import 'package:flutter_mvvm_architecture/screens/home/article_repository.dart';
import 'package:flutter_mvvm_architecture/shared/services/connectivity_service.dart';
import 'package:flutter_mvvm_architecture/shared/services/rest_service.dart';
import 'package:flutter_mvvm_architecture/utils/constants.dart';
import 'package:flutter_mvvm_architecture/utils/custom_json_converter.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

void initLocator() {
  // TODO examine undisposed services and how they impact performance after app has been killed
  // Here lies chopper client and its services
  locator.registerLazySingleton<ChopperClient>(
    () => ChopperClient(
      baseUrl: Constants.baseUrl,
      services: [
        RestService.create(),
      ],
      interceptors: [
        // HttpLoggingInterceptor(),
        CurlInterceptor(),
      ],
      converter: const CustomJsonConverter(),
    ),
  );
  locator.registerLazySingleton<RestService>(
      () => RestService.create(locator<ChopperClient>()));
  locator
      .registerLazySingleton<ConnectivityService>(() => ConnectivityService());
  locator.registerLazySingleton<ArticleRepository>(() => ArticleRepository());
}

void initHive() {
  Hive.initFlutter();
}
