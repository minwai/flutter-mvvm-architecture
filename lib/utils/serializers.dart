import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:flutter_mvvm_architecture/shared/models/article.dart';
import 'package:flutter_mvvm_architecture/shared/models/article_wrapper.dart';

part 'serializers.g.dart';

/// TODO initialize every built object inside brackets for serialization
@SerializersFor([ArticleWrapper, Article])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
