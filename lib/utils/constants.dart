/// Make constant files abstract to prevent from getting instantiated

abstract class Constants {
  static const String baseUrl = "http://128.199.178.229:3030";

  static const String csrfKey = "x-csrf-token";
  static const String defaultColumns = "article.*,articlecategory.category";
  static const String defaultJoin =
      "inner,articlecategory,article_category,eq,articlecategory.id";
  static const String defaultFilter = "status,eq,1%s";
  static const String defaultOrder = "id,desc";
  static const String defaultPageRange = "0,10";
}
