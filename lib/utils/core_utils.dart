import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

T getStore<T extends Store>(BuildContext context) {
  return Provider.of<T>(context, listen: false);
}
